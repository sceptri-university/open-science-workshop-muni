let
  pkgs = import <nixpkgs> {};
  lib = import <lib> {};

  config = import <config> {};
  configPath = /home/sceptri/Documents/Dev/homelab/.;

  quartoPreRelease = extraRPackages:
    pkgs.callPackage (configPath + "/packages/quarto/preRelease.nix")
    (with pkgs lib config; {extraRPackages = extraRPackages;});

  catterplots = pkgs.rPackages.buildRPackage rec {
    name = "CatterPlots-${version}";
    version = "0.0.2";
    requireX = false;
    src = pkgs.fetchFromGitHub {
      owner = "Gibbsdavidl";
      repo = "CatterPlots";
      rev = "ae17cd5e49ddda4ecfe0eba8a4c21df8c88e72c4";
      sha256 = "sha256-z3dSw/MWjhDK4KRNNSzEn23dSlbysY5ee/06R32kSGE=";
    };
    buildInputs = with pkgs.rPackages; [pkgs.R devtools png];
  };

  rPackages = with pkgs.rPackages; [
    quarto
    devtools
    png
    languageserver
    markdown
    catterplots
  ];
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      texlive.combined.scheme-full
      pandoc
      xclip
      (
        rWrapper.override {
          packages = rPackages;
        }
      )
      (
        rstudioWrapper.override {
          packages = rPackages;
        }
      )
      (
        quartoPreRelease rPackages
      )
    ];
    shellHook = ''
      quarto install extension jmbuhr/quarto-qrcode \
      echo "Welcome to the Git Good Workshop shell"
    '';
  }
